use std::fs::File;
use std::io::{
    BufReader,
    BufRead,
};

fn main() -> Result<(), String> {
    let file_name = std::env::args()
        .nth(1)
        .unwrap_or("day8".to_owned());
    parse(&file_name)?;
    Ok(())
}

fn parse(file_name: &str) -> Result<(), String> {
    println!("filename: {}", file_name);
    let f = File::open(format!("testdata/{}.txt", file_name)).expect("Couldn't open file");
    let f = BufReader::new(f);

    let mut strsize = 0;
    let mut memsize = 0;

    for line in f.lines() {
        match line {
            Err(_) => return Err("Something went wrong reading the file".to_string()),
            Ok(line) => {
                println!("Parsing: {} - {}", line.len(), line);
                let unescaped = escape(&line)?;
                println!("escaped: {}", unescaped);
                strsize += line.len();
                memsize += unescaped;
            },
        }
    }
    println!("Total: {}", memsize - strsize);

    Ok(())
}

fn escape(l: &str) -> Result<usize, String> {
    let mut chars = l.chars();
    let mut counter = 0;

    loop {
        let c = chars.next();
        if let None = c {
            break;
        }

        let increase = match c {
            Some('"')  => 2,
            Some('\\') => 2,
            Some(_)    => 1,
            None       => 0,
        };
        counter += increase;
    }

    Ok(counter + 2)
}

