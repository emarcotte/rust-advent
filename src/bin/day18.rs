use std::fs::File;
use std::io::Read;

fn main() -> Result<(), String> {
    let mut input = String::new();
    File::open("testdata/day18.txt")
        .or(Err("Couldn't open file"))?
        .read_to_string(&mut input)
        .or(Err("Failed to read file"))?;

    let (w, h, mut grid) = get_grid(&input);

    for i in 0..100 {
        grid = life(w, h, &grid);
        println!("Iter: {}", i);
        for y in 0..h {
            for c in grid.get(y*w..y*w + w).unwrap() {
                print!("{}", match c { 0 => '.', 1 => '#', _ => 'x' });
            }
            println!();
        }
        println!("On: {}", grid.iter().filter(|c| **c == 1).count());

    }



    Ok(())
}

fn get_grid(input: &str) -> (usize, usize, Vec<u32>) {
    let mut grid = Vec::new();
    let mut height = 0;
    for line in input.lines() {
        height += 1;
        for char in line.chars() {
            match char {
                '.' => grid.push(0),
                '#' => grid.push(1),
                _   => {},
            }
        }
    }
    (grid.len() / height, height, grid)
}

fn life(width: usize, height: usize, grid: &[u32]) -> Vec<u32> {
    let mut new_grid = grid.to_vec();

    for x in 0..width {
        for y in 0..height {
            let c = x + y * width;
            if (x == width - 1 || x == 0) && (y == 0 || y == height-1) {
                new_grid[c] = 1;
            }
            else {
                let neighbors = count(width, height, &grid, x, y);
                if grid[c] == 1 && !(neighbors == 2 || neighbors == 3) {
                    new_grid[c] = 0;
                }

                if grid[c] == 0 && neighbors == 3 {
                    new_grid[c] = 1;
                }
            }
        }
    }
    new_grid
}

fn count(width: usize, height: usize, grid: &[u32], x: usize, y: usize) -> u32 {
    let mut c = 0;
    if x > 0 {
        c += grid[x-1 + y * width];
        if y < height - 1 {
            c += grid[x-1 + (y + 1) * width];
        }
        if y > 0 {
            c += grid[x-1 + (y -1) * width];
        }
    }

    if x < width - 1 {
        c += grid[x + 1 + y * width];
        if y < height - 1 {
            c += grid[x + 1 + (y + 1) * width];
        }
        if y > 0 {
            c += grid[x + 1 + (y -1) * width];
        }
    }

    if y < height - 1 {
        c += grid[x + (y + 1) * width];
    }

    if y > 0 {
        c += grid[x + (y -1) * width];
    }

    c
}
