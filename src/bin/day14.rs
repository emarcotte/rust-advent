use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::cmp::{ max, min };

#[derive(Debug)]
struct Deer {
    name: String,
    speed: u32,
    duration: u32,
    rest: u32,
    cycle: u32,
    cycle_dist: u32,
}

impl Deer {
    fn new(name: String, speed: u32, duration: u32, rest: u32) -> Deer {
        Deer{
            name: name,
            speed,
            duration,
            rest,
            cycle: duration + rest,
            cycle_dist: speed * duration,
        }
    }

    pub fn distance(&self, seconds: u32) -> u32 {
        let cycles = seconds / self.cycle;
        let partial = min(seconds % self.cycle, self.duration);
        cycles * self.cycle_dist + partial * self.speed
    }
}

fn main() -> Result<(), String> {
    let mut input = String::new();
    File::open("testdata/day14.txt")
        .or(Err("Couldn't open file"))?
        .read_to_string(&mut input)
        .or(Err("Failed to read file"))?;

    let deer = 
    get_deer(&input);
    /*
    vec!(
        Deer::new("Comet".to_string(), 14, 10, 127),
        Deer::new("Dancer".to_string(), 16, 11, 162),
    );
    */

    let mut scores: HashMap<&String, u32> = HashMap::new();
    let mut best = vec!();

    for time in 1..=2503{
        best.clear();
        for d in deer.iter() {
            best.push((&d.name, d.distance(time) as i64));
        }

        let leading_dist = best.iter().fold(0, |f, b| max(f, b.1));
        for b in best.iter() {
            if b.1 == leading_dist {
                scores.entry(&b.0)
                    .and_modify(|v| *v += 1)
                    .or_insert(1);
            }
        }
    }

    println!("{:?}", scores);

    Ok(())
}

fn get_deer(input: &str) -> Vec<Deer> {
    let mut deer = vec!();

    for line in input.lines() {
        let mut tokens = line.split(' ');
        let who = tokens.nth(0)
            .unwrap()
            .to_string();

        let speed = tokens.nth(2)
            .unwrap()
            .parse::<u32>()
            .unwrap();

        let duration = tokens.nth(2)
            .unwrap()
            .parse::<u32>()
            .unwrap();

        let rest = tokens.nth(6)
            .unwrap()
            .parse::<u32>()
            .unwrap();

        deer.push(Deer::new(who, speed, duration, rest));
    }

    deer
}
