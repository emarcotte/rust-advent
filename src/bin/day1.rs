use std::fs::File;
use std::io::Read;

fn main() {
    let mut f = File::open("testdata/day1.txt").expect("Couldn't open file");
    let mut content = String::new();
    f.read_to_string(&mut content).expect("Couldn't read file");
    let mut floor = 0;
    let mut index = 0;
    for c in content.chars() {
        floor += match c { '(' => 1, ')' => -1, _ => 0, };
        index += 1;
        if floor < 0 {
            break;
        }
    }
        
    println!("Floor: {}, index: {}", floor, index);
}
