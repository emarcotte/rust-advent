fn main() -> Result<(), String> {
    let mut input = "1113122113".to_string();

    for n in 0..50 {
        input = expand(&input)?;
        println!("{}, {}", n, input.len());
    }

    Ok(())
}

#[test]
fn test_expand() {
    assert_eq!(expand("1"), Ok("11".to_string()));
    assert_eq!(expand("11"), Ok("21".to_string()));
    assert_eq!(expand("21"), Ok("1211".to_string()));
    assert_eq!(expand("1211"), Ok("111221".to_string()));
    assert_eq!(expand("111221"), Ok("312211".to_string()));
}

fn expand(input: &str) -> Result<String, String> {
    let mut iter = input.chars();
    let n = match iter.next() {
        None    => Err("missing token"),
        Some(v) => match v.to_digit(10) {
            Some(n) => Ok(n),
            None    => Err("Not a digit"),
        },
    }?;

    let mut current = (n, 1);

    let mut new_string = String::with_capacity(input.len() * 2);

    loop {

        match iter.next() {
            None    => {
                new_string += &format!("{}{}", current.1, current.0);
                break;
            },
            Some(c) => match c.to_digit(10) {
                Some(d) => {
                    if current.0 == d {
                        current.1 += 1;
                    }
                    else {
                        new_string += &format!("{}{}", current.1, current.0);
                        current = (d, 1);
                    }
                },
                None => {},
            }
        }
    }

    Ok(new_string)
}
