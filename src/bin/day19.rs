use std::fs::File;
use std::io::Read;
use std::collections::{ HashMap, HashSet };

fn main() -> Result<(), String> {
    let mut input = String::new();
    File::open("testdata/day19.txt")
        .or(Err("Couldn't open file"))?
        .read_to_string(&mut input)
        .or(Err("Failed to read file"))?;

    let (replacements, starter) = get_data(&input);

    let permutations = permute(&starter, &replacements);

    println!("P: {:?}", permutations.len());

    Ok(())
}

fn permute(sequence: &str, replacements: &HashMap<String, Vec<String>>) -> HashSet<String> {
    let mut rv = HashSet::new();
    for search in replacements.keys() {
        let with = &replacements[search];

        for index in sequence.match_indices(search) {
            for r in with {
                rv.insert(
                    format!(
                        "{}{}{}",
                        match sequence.get(0..index.0) { Some(x) => x, _ => "" },
                        r,
                        match sequence.get(search.len() + index.0..) { Some(x) => x, _ => "" }
                    )
                );
            }
        }
    }
    rv
}

fn get_data(input: &str) -> (HashMap<String, Vec<String>>, String) {
    let mut replacements = HashMap::<String, Vec<String>>::new();
    let mut starter = String::new();
    for line in input.lines() {
        let mut replacement = line.split(" => ");
        let lhs = replacement.nth(0).unwrap().to_string();
        let rhs = replacement.nth(0);
        if let Some(rhs) = rhs {
            replacements.entry(lhs)
                .and_modify(|v| v.push(rhs.to_string()))
                .or_insert_with(|| vec!(rhs.to_string()));
        }
        else if line.len() > 0 {
            starter = line.to_string();
        }
    }

    (replacements, starter)
}
