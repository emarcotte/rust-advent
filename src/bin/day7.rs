use std::fs::File;
use std::io::{
    BufReader,
    BufRead,
};
use std::collections::HashMap;
use std::cell::RefCell;

fn main() -> Result<(), String> {
    let file_name = std::env::args()
        .nth(1)
        .unwrap_or("day7".to_owned());
    let mut b = Board::parse(&file_name)?;
    b.parse_instruction("956 -> b")?;
    /*
    let b = Board::parse(r#"123 -> x
NOT x -> y
1 -> a
2 -> b
a AND b -> c
a OR b -> d
a LSHIFT 1 -> e
b RSHIFT 1 -> f
a LSHIFT 2 -> g"#)?;
*/
    let mut wires = b.wires();
    wires.sort();
    for wire in wires {
        println!("{} = {:?}", wire, b.get(wire).and_then(|w| w.eval(&b)))
    }
    Ok(())
}

#[derive(Clone)]
enum Placeholder {
    WireName(String),
    Value(u16),
}
    ///123 -> x
    ///456 -> y
    ///x AND y -> d
    ///x OR y -> e
    ///x LSHIFT 2 -> f
    ///y RSHIFT 2 -> g
    ///NOT x -> h

trait WireOp {
    fn eval(&self, board: &Board) -> Option<u16>;
}

struct NoOp {
    input: Placeholder,
}

impl WireOp for NoOp {
    fn eval(&self, board: &Board) -> Option<u16> {
        board.lookup(&self.input)
    }
}

struct AndOp {
    left: Placeholder,
    right: Placeholder,
}

impl WireOp for AndOp {
    fn eval(&self, board: &Board) -> Option<u16> {
        let right = board.lookup(&self.right);
        let left = board.lookup(&self.left);
        if !(left.is_some() && right.is_some()) {
            return None;
        }

        Some(left.unwrap() & right.unwrap())
    }
}

struct OrOp {
    left: Placeholder,
    right: Placeholder,
}

impl WireOp for OrOp {
    fn eval(&self, board: &Board) -> Option<u16> {
        let right = board.lookup(&self.right);
        let left = board.lookup(&self.left);
        if !(left.is_some() && right.is_some()) {
            return None;
        }

        Some(left.unwrap() | right.unwrap())
    }
}

struct LShiftOp {
    right: Placeholder,
    left: Placeholder,
}

impl WireOp for LShiftOp {
    fn eval(&self, board: &Board) -> Option<u16> {
        let right = board.lookup(&self.right);
        let left = board.lookup(&self.left);
        if !(left.is_some() && right.is_some()) {
            return None;
        }

        Some(left.unwrap() << right.unwrap())
    }
}

struct RShiftOp {
    right: Placeholder,
    left: Placeholder,
}

impl WireOp for RShiftOp {
    fn eval(&self, board: &Board) -> Option<u16> {
        let right = board.lookup(&self.right);
        let left = board.lookup(&self.left);
        if !(left.is_some() && right.is_some()) {
            return None;
        }

        Some(left.unwrap() >> right.unwrap())
    }
}

struct NegationOp {
    input: Placeholder,
}

impl WireOp for NegationOp {
    fn eval(&self, board: &Board) -> Option<u16> {
        board.lookup(&self.input)
            .and_then(|v| Some(!v))
    }
}

struct Board {
    wires: HashMap<String, Box<WireOp>>,
    cache: RefCell<HashMap<String, Option<u16>>>,
}

impl Board {
    fn new() -> Board {
        Board {
            wires: HashMap::new(),
            cache: RefCell::new(HashMap::new()),
        }
    }

    fn lookup(&self, p: &Placeholder) -> Option<u16> {
        match p {
            Placeholder::WireName(name) => {
                if self.cache.borrow().contains_key(name) {
                    return *self.cache.borrow().get(name).unwrap();
                }

                let v = self.get(name)
                    .and_then(|op| op.eval(self));
                self.cache.borrow_mut().insert(name.to_string(), v);
                return v;
            },
            Placeholder::Value(v) => Some(*v),
        }
    }

    fn add_op(&mut self, name: &str, op: Box<WireOp>) {
        self.wires.insert(name.to_owned(), op);
    }

    fn wires(&self) -> Vec<&String> {
        self.wires.keys().collect()
    }

    fn get(&self, name: &str) -> Option<&Box<WireOp>> {
        self.wires.get(name)
    }

    fn parse(file_name: &str) -> Result<Board, String> {
        println!("filename: {}", file_name);
        let f = File::open(format!("testdata/{}.txt", file_name)).expect("Couldn't open file");
        let f = BufReader::new(f);


        let mut b = Board::new();

        for line in f.lines() {
            match line {
                Err(_) => return Err("Something went wrong reading the file".to_string()),
                Ok(line) => {
                    println!("Parsing: {}", line);
                    b.parse_instruction(&line)?;
                },
            }
        }

        Ok(b)
    }

    fn parse_val(&mut self, tokens: &mut Vec<&str>) -> Result<Placeholder, String> {
        if let Some(n_or_wire) = tokens.pop() {
            return match n_or_wire.parse::<u16>() {
                Ok(n)   => Ok(Placeholder::Value(n)),
                Err(_)  => Ok(Placeholder::WireName(n_or_wire.to_string())),
            };
        }
        Err("Could not find next value".to_string())
    }

    ///123 -> x
    ///456 -> y
    ///x AND y -> d
    ///x OR y -> e
    ///x LSHIFT 2 -> f
    ///y RSHIFT 2 -> g
    ///NOT x -> h
    fn parse_instruction(&mut self, inst: &str) -> Result<(), String> {
        let mut tokens: Vec<_> = inst.split(' ').collect();

        if let Some(dest) = tokens.pop() {
            let dest = dest.to_string();
            tokens.pop();

            match self.parse_val(&mut tokens) {
                Err(e) => return Err(e),
                Ok(right_val) => {
                    match tokens.pop() {
                        Some(op) => match op {
                            "AND"    => {
                                let left = self.parse_val(&mut tokens)?;
                                self.add_op(&dest, Box::new(AndOp {
                                    left: left,
                                    right: right_val,
                                }))
                            },
                            "OR"     => {
                                let left = self.parse_val(&mut tokens)?;
                                self.add_op(&dest, Box::new(OrOp {
                                    left: left,
                                    right: right_val,
                                }))
                            },
                            "LSHIFT" => {
                                let left = self.parse_val(&mut tokens)?;
                                self.add_op(&dest, Box::new(LShiftOp {
                                    left: left,
                                    right: right_val,
                                }))
                            },
                            "RSHIFT" => {
                                let left = self.parse_val(&mut tokens)?;
                                self.add_op(&dest, Box::new(RShiftOp {
                                    left,
                                    right: right_val,
                                }))
                            },
                            "NOT"    => self.add_op(&dest, Box::new(NegationOp {
                                input: right_val,
                            })),
                            _        => return Err(format!("Unknown operation: {}", op)),
                        },
                        None  => self.add_op(&dest, Box::new(NoOp {
                            input: right_val,
                        })),
                    };

                    /*
                    println!("Dest: {}", dest);
                    println!("Right: {}", match right_val {
                        Placeholder::WireName(w) => w,
                        Placeholder::Value(v) => v.to_string(),
                    });
                    */
                },
            };

            return Ok(());
        }

        Err ("Could not parse dest".to_string())
    }
}

