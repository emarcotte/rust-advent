use std::collections::HashMap;
use std::fs::File;
use std::io::Read;

type Ruleset = HashMap<String, HashMap<String, i32>>;

fn main() -> Result<(), String> {
    let mut input = String::new();
    File::open("testdata/day13.txt")
        .or(Err("Couldn't open file"))?
        .read_to_string(&mut input)
        .or(Err("Failed to read file"))?;

    let rules = get_rules(&input);
    let mut members: Vec<_> = rules.keys().collect();
    let myself = "Myself".to_string();
    members.push(&myself);

    println!("Best: {}", find_arrangement(&rules, &members, &mut vec!()));

    Ok(())
}

fn find_arrangement(rules: &Ruleset, standing: &Vec<&String>, seated: &mut Vec<String>) -> i32 {
    let mut best = 0;
    for member in standing {
        let remaining = standing.iter()
            .filter_map(|e| {
                if e != member {
                    return Some(*e);
                }
                None
            })
            .collect::<Vec<_>>();

        seated.push(member.to_string());
        let happiness;
        if remaining.len() != 0 {
            happiness = find_arrangement(rules, &remaining, seated);
        }
        else {
            happiness = score(rules, &seated);
        }

        if best < happiness {
            best = happiness;
        }

        seated.pop();
    }
    best
}

fn score(rules: &Ruleset, seated: &Vec<String>) -> i32 {
    let mut iter = seated.iter();
    let first = iter.nth(0).unwrap();
    let mut prev = first;
    println!("SCoring: {:?}", seated);

    let mut score = 0;

    for w in iter {
        if let Some(r) = rules.get(w) {
            if let Some(h) = r.get(prev) {
                score += h;
            }
        }

        if let Some(r) = rules.get(prev) {
            if let Some(h) = r.get(w) {
                score += h;
            }
        }

        prev = w;
    }

    if let Some(r) = rules.get(first) {
        if let Some(h) = r.get(prev) {
            score += h;
        }
    }

    if let Some(r) = rules.get(prev) {
        if let Some(h) = r.get(first) {
            score += h;
        }
    }

    score
}

fn get_rules(input: &str) -> Ruleset {
    let mut rules = Ruleset::new();

    for line in input.lines() {
        let mut tokens = line.split(' ');
        let who = tokens.nth(0)
            .unwrap()
            .to_string();

        let direction = match tokens.nth(1).unwrap() {
            "lose" => -1,
            "gain" => 1,
            _      => 0,
        };

        let happiness = tokens.nth(0)
            .unwrap()
            .parse::<i32>()
            .unwrap();


        let next_to = tokens.nth(6)
            .unwrap()
            .to_string();

        let next_to = next_to.get(0..next_to.len() - 1)
            .unwrap()
            .to_string();

        rules.entry(who)
            .or_insert_with(|| HashMap::new())
            .insert(next_to, happiness * direction);
    }

    rules
}

#[test]
fn test_get_rules() {
    let input = r#"Alice would gain 54 happiness units by sitting next to Bob.
Alice would lose 79 happiness units by sitting next to Carol.
Alice would lose 2 happiness units by sitting next to David.
Bob would gain 83 happiness units by sitting next to Alice.
Bob would lose 7 happiness units by sitting next to Carol.
Bob would lose 63 happiness units by sitting next to David.
Carol would lose 62 happiness units by sitting next to Alice.
Carol would gain 60 happiness units by sitting next to Bob.
Carol would gain 55 happiness units by sitting next to David.
David would gain 46 happiness units by sitting next to Alice.
David would lose 7 happiness units by sitting next to Bob.
David would gain 41 happiness units by sitting next to Carol."#;

    let rules = get_rules(&input);
    println!("{:?}", rules);
    assert_eq!(rules["Alice"]["Bob"], 54);

}

