use std::cmp::min;

fn main() -> Result<(), String> {
    let target = 29_000_000 / 11;
    let mut houses: Vec<usize> = vec![0; target];

    let mut found = target;

    for elf in 1..target {
        for house in (elf ..= min(target, elf * 50)).step_by(elf) {
            houses[house] += elf;
            if houses[house] >= target && house < found {
                found = house;
            }
        }
    }
    
    println!("House: {}", found);

    Ok(())
}
