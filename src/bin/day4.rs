use crypto::md5::Md5;
use crypto::digest::Digest;
use std::sync::atomic::{Ordering, AtomicUsize};
use std::sync::Arc;

use crossbeam;

fn main() {
    let input: String = std::env::args()
        .nth(1)
        .unwrap_or("yzbqklnj".to_owned());

    let threads: usize = 8;
    let n = std::u32::MAX / threads as u32;


    let shared = Arc::new(AtomicUsize::new(threads+1));

    // TODO stop threads when smaller number found:
    crossbeam::scope(|spawner| {
        let mut handles = Vec::with_capacity(threads);

        for thread in 0..threads {
            let input = input.clone();
            let shared = shared.clone();
            handles.push(
                spawner.spawn(move |_| {
                    let mut result = [0u8;16];
                    let mut md5 = Md5::new();
                    for n in thread as u32 *n..thread as u32*n+n {
                        if shared.load(Ordering::Relaxed) < thread {
                            return None

                        }
                        md5.input(input.as_bytes());
                        md5.input(n.to_string().as_bytes());
                        md5.result(&mut result);
                        md5.reset();
                        if result[0] == 0 && result[1] == 0 && result[2] == 0 {
                            println!("n: {} - {:?}", n, result);
                            shared.swap(thread, Ordering::Relaxed);
                            return Some(n);
                        }
                    }
                    None
                })
            );
        }
        let results: Vec<_> = handles.into_iter().map(|h| h.join()).collect();
        println!("{:?}", results);
    }).expect("Threads crashed :(");
}
