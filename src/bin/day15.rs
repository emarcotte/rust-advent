use std::fs::File;
use std::io::Read;
use std::cmp::{ max, };

#[derive(Debug)]
struct Ingredient {
    name: String,
    capacity: i32,
    durability: i32,
    flavor: i32,
    texture: i32,
    calories: i32
}

fn main() -> Result<(), String> {
    let mut input = String::new();
    File::open("testdata/day15.txt")
        .or(Err("Couldn't open file"))?
        .read_to_string(&mut input)
        .or(Err("Failed to read file"))?;

    let ingredients = get_ingredients(&input);



    let spoon_combos = spoons(ingredients.len(), 100)
         .into_iter()
         .filter(|sub| sub.iter().fold(0, |acc, s| acc + s) == 100)
         .collect::<Vec<Vec<i32>>>();

    let mut m = 0;
    for combo in spoon_combos {
        let s = score(&ingredients, &combo);
        println!("{:?} {}", combo, s);
        m = max(m, s);
    }
    println!("Score: {}", m);


    Ok(())
}

fn spoons(s: usize, m: i32) -> Vec<Vec<i32>> {
    if s == 1 {
        return (0..=m).map(|e| vec!(e)).collect::<Vec<Vec<i32>>>();
    }

    let mut r = Vec::new();
    let subs = spoons(s-1, m);
    for sub in subs {
        let score = sub.iter().fold(0, |acc, s| acc + s);
        let remain = m - score;
        for i in 0..=remain {
            let mut n = sub.clone();
            n.push(i);
            r.push(n);
        }
    }
    r
}

fn score(ingredients: &[Ingredient], spoons: &[i32]) -> i32 {
    let capacity = max(0, ingredients.iter().zip(spoons.iter())
        .fold(0, |acc, (i, s)| acc + i.capacity * *s));
    let durability = max(0, ingredients.iter().zip(spoons.iter())
        .fold(0, |acc, (i, s)| acc + i.durability * *s));
    let flavor = max(0, ingredients.iter().zip(spoons.iter())
        .fold(0, |acc, (i, s)| acc + i.flavor * *s));
    let texture = max(0, ingredients.iter().zip(spoons.iter())
        .fold(0, |acc, (i, s)| acc + i.texture * *s));
    let calories = max(0, ingredients.iter().zip(spoons.iter())
        .fold(0, |acc, (i, s)| acc + i.calories * *s));

    if calories == 500 {
        capacity * durability * flavor * texture
    }
    else {
        0
    }
}

fn get_ingredients(input: &str) -> Vec<Ingredient> {
    let mut deer = vec!();

    for line in input.lines() {
        let line = line.replace(',', " ");
        let mut tokens = line.split(' ');
        let name: String = tokens.nth(0)
            .unwrap()
            .to_string();

        let name = name.get(0..name.len()-1)
            .unwrap()
            .to_string();

        let capacity = tokens.nth(1)
            .unwrap()
            .parse::<i32>()
            .unwrap();

        let durability = tokens.nth(2)
            .unwrap()
            .parse::<i32>()
            .unwrap();

        let flavor = tokens.nth(2)
            .unwrap()
            .parse::<i32>()
            .unwrap();

        let texture = tokens.nth(2)
            .unwrap()
            .parse::<i32>()
            .unwrap();

        let calories = tokens.nth(2)
            .unwrap()
            .parse::<i32>()
            .unwrap();

        deer.push(
            Ingredient {
                name,
                capacity,
                durability,
                flavor,
                texture,
                calories,
            }
        );
    }

    deer
}
