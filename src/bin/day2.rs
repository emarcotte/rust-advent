use std::fs::File;
use std::io::{BufReader, BufRead};
use std::cmp::{min};

fn main() {
    let file_name = std::env::args().nth(1).unwrap_or("day2".to_owned());
    println!("filename: {}", file_name);
    let f = File::open(format!("testdata/{}.txt", file_name)).expect("Couldn't open file");
    let f = BufReader::new(f);
    let mut order = 0;
    let mut ribbon_order = 0;

    for line in f.lines() {
        let line: String = line.unwrap();
        let mut split = line.split('x');
        let l = split.next().unwrap().parse::<u32>().unwrap();
        let w = split.next().unwrap().parse::<u32>().unwrap();
        let h = split.next().unwrap().parse::<u32>().unwrap();
        let dims = [l*w, l*h, w*h];
        let mut actual_size = 0;
        let mut smalltest_side = std::u32::MAX;
        let mut edges = [l, w, h];
        edges.sort();
        let (s1, s2) = (edges[0], edges[1]);
        dims.into_iter().for_each(|d| {
            actual_size += 2*d;
            smalltest_side = min(smalltest_side, *d);
        });
        ribbon_order += s1*2 + s2*2 + l * w *h;
        println!("Total: {}, small: {}", actual_size, smalltest_side);
        let total_size = smalltest_side + actual_size;
        order += total_size;
    }
    println!("Order size: {}, ribbon {}", order, ribbon_order);
}
