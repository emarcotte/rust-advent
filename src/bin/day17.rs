use std::cmp::{min};

fn main() -> Result<(), String> {
    let mut containers = vec!( 11, 30, 47, 31, 32, 36, 3, 1, 5, 3, 32, 36, 15, 11, 46, 26, 28, 1, 19, 3);
    //let mut containers = vec!( 20, 15, 10, 5, 5);
    containers.sort();

    let combos = store(150, &containers, 0);
    let smallest_combo_size = combos.iter()
        .map(|c| c.iter().filter(|cup| **cup > 0).count())
        .fold(containers.len(), |a, s| min(a, s));
    println!("Smallest combo: {}", smallest_combo_size);
    println!("{:?}", combos.iter()
             .filter(|c| c.iter().filter(|cup| **cup > 0).count() == smallest_combo_size)
             .count());

    Ok(())
}

fn store(s: u32, c: &Vec<u32>, b: usize) -> Vec<Vec<u32>> {
    let cup = c[b];
    if b == c.len() - 1 {
        return vec!(vec!(0), vec!(cup));
    }

    let mut r = Vec::new();
    let subs = store(s, c, b + 1);
    for sub in subs {
        let used = sub.iter().fold(0, |acc, s| acc + s);
        let remain = s - used;
        if remain >= cup {
            let mut n = sub.clone();
            n.push(cup);
            r.push(n);
        }
        let mut n = sub.clone();
        n.push(0);
        r.push(n);
    }

    if b == 0  {
        r.into_iter().filter(|sub| {
            s == sub.iter().fold(0, |acc, s| acc + s)
        }).collect()
    }
    else {
        r
    }
}
