use std::fs::File;
use std::io::{BufReader, BufRead};

fn main() -> Result<(), String> {
    let file_name = std::env::args()
        .nth(1)
        .unwrap_or("day6".to_owned());
    println!("filename: {}", file_name);

    let cols = 1000;
    let rows = 1000;
    let mut grid: Grid = (0..cols*rows)
        .map(|_| 0)
        .collect();

    let f = File::open(format!("testdata/{}.txt", file_name)).expect("Couldn't open file");
    let f = BufReader::new(f);

    for line in f.lines() {
        let line: String = line.unwrap();
        instruction(&mut grid, cols, &line).expect("Failure!");
    }

    let mut brightness = 0;
    for i in grid {
        brightness += i;
    }

    println!("On: {}", brightness);
    Ok(())
}

type Grid = Vec<usize>;
type CoordRange = (Coord, Coord);
type Coord = (usize, usize);
type GridOp = fn(&mut Grid, usize, CoordRange);

fn parse_instruction(inst: &str) -> Result<(GridOp, CoordRange), String> {
    let tokens: Vec<_> = inst.split(' ').collect();
    if tokens[0] == "toggle" {
        return Ok((toggle, parse_range(tokens[1], tokens[3])?));
    }
    else if tokens[1] == "on" {
        return Ok((enable, parse_range(tokens[2], tokens[4])?));
    }
    else if tokens[1] == "off" {
        return Ok((disable, parse_range(tokens[2], tokens[4])?));
    }
    Err("Could not determine instruction type".to_string())
}

fn parse_coord(a: &str) -> Result<Coord, String> {
    let mut tokens = a.split(',');
    match tokens.next().map(|e| e.parse::<usize>()) {
        Some(Ok(a)) => {
            match tokens.next().map(|e| e.parse::<usize>()) {
                Some(Ok(b)) => Ok((a, b)),
                Some(Err(e)) => Err(e.to_string()),
                None => Err("Missing token b".to_string()),
            }
        },
        Some(Err(e)) => Err(e.to_string()),
        None => Err("Missing token a".to_string()),
    }
}

fn parse_range(a: &str, b: &str) -> Result<CoordRange, String> {
    Ok((parse_coord(a)?, parse_coord(b)?))
}

fn instruction(grid: &mut Grid, cols: usize, inst: &str) -> Result<(), String> {
    match  parse_instruction(inst) {
        Ok((op, range)) => {
            op(grid, cols, range);
            Ok(())
        },
        Err(e) => Err(format!("Warning could not parse: ({}) because: {}", inst, e)),
    }
}

fn disable(grid: &mut Grid, cols: usize, ((ac, ar), (bc, br)): CoordRange) {
    for row in ar..=br {
        let base = cols * row;
        for row in ac..=bc {
            if grid[base + row] > 0 {
                grid[base + row] -= 1
            }
        }
    }
}

fn enable(grid: &mut Grid, cols: usize, ((ac, ar), (bc, br)): CoordRange) {
    for row in ar..=br {
        let base = cols * row;
        for row in ac..=bc {
            grid[base + row] += 1;
        }
    }
}

fn toggle(grid: &mut Grid, cols: usize, ((ac, ar), (bc, br)): CoordRange) {
    for row in ar..=br {
        let base = cols * row;
        for row in ac..=bc {
            grid[base + row] += 2;
        }
    }
}
