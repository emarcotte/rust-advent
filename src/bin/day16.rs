use std::fs::File;
use std::io::Read;

#[derive(Debug)]
struct Sue {
    number: u32,
    cats: Option<u32>,
    samoyeds: Option<u32>,
    pomeranians: Option<u32>,
    akitas: Option<u32>,
    vizslas: Option<u32>,
    goldfish: Option<u32>,
    trees: Option<u32>,
    cars: Option<u32>,
    perfumes: Option<u32>,
}

fn main() -> Result<(), String> {
    let mut input = String::new();
    File::open("testdata/day16.txt")
        .or(Err("Couldn't open file"))?
        .read_to_string(&mut input)
        .or(Err("Failed to read file"))?;

    let sues = get_sues(&input);

    let criteria = Sue {
        number: 0,
        cats: Some(7),
        samoyeds: Some(2),
        pomeranians: Some(3),
        akitas: Some(0),
        vizslas: Some(0),
        goldfish: Some(5),
        trees: Some(3),
        cars: Some(2),
        perfumes: Some(1),
    };

    let mut best = &sues[0];
    let mut best_score = 0;

    for sue in sues.iter() {
        let mut temp_score = 0;
        if criteria.cats < sue.cats { temp_score += 1 } else if sue.cats.is_some() { continue; }
        if criteria.samoyeds == sue.samoyeds { temp_score += 1 } else if sue.samoyeds.is_some() { continue; }
        if criteria.pomeranians > sue.pomeranians { temp_score += 1 } else if sue.pomeranians.is_some() { continue; }
        if criteria.akitas == sue.akitas { temp_score += 1 } else if sue.akitas.is_some() { continue; }
        if criteria.vizslas == sue.vizslas { temp_score += 1 } else if sue.vizslas.is_some() { continue; }
        if criteria.goldfish > sue.goldfish { temp_score += 1 } else if sue.goldfish.is_some() { continue; }
        if criteria.trees < sue.trees { temp_score += 1 } else if sue.trees.is_some() { continue; }
        if criteria.cars == sue.cars { temp_score += 1 } else if sue.cars.is_some() { continue; }
        if criteria.perfumes == sue.perfumes { temp_score += 1 } else if sue.perfumes.is_some() { continue; }
        
        if temp_score > best_score {
            best = sue;
            best_score = temp_score;
        }
    }

    println!("{:?}", best);

    Ok(())
}

fn get_sues(input: &str) -> Vec<Sue> {
    let mut sues = vec!();

    for line in input.lines() {
        let line = line.replace(',', "")
            .replace(':', "");
        let mut tokens = line.split(' ');
        let num = tokens.nth(1)
            .unwrap()
            .parse::<u32>()
            .unwrap();

        let mut sue = Sue {
            number: num,
            cats: None,
            samoyeds: None,
            pomeranians: None,
            akitas: None,
            vizslas: None,
            goldfish: None,
            trees: None,
            cars: None,
            perfumes: None,
        };

        for _ in 0..3 {
            let prop = tokens.nth(0)
                .unwrap();
            let v = tokens.nth(0)
                .unwrap()
                .parse::<u32>()
                .unwrap();
            if prop == "cats" { sue.cats = Some(v) };
            if prop == "samoyeds" { sue.samoyeds = Some(v) };
            if prop == "pomeranians" { sue.pomeranians = Some(v) };
            if prop == "akitas" { sue.akitas = Some(v) };
            if prop == "vizslas" { sue.vizslas = Some(v) };
            if prop == "goldfish" { sue.goldfish = Some(v) };
            if prop == "trees" { sue.trees = Some(v) };
            if prop == "cars" { sue.cars = Some(v) };
            if prop == "perfumes" { sue.perfumes = Some(v) };
        }

        sues.push(sue);
    }

    sues
}
