use std::fs::File;
use std::io::{
    BufReader,
    BufRead,
};

use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

struct Peer {
    node: Rc<Node>,
    dist: u32,
}

struct Node {
    name: String,
    peers: RefCell<Vec<Peer>>,
}

impl Node {
    fn new(name: &str) -> Node {
        Node {
            name: name.to_string(),
            peers: RefCell::new(vec!()),
        }
    }

    fn add_peer(&self, node: Rc<Node>, dist: u32) {
        self.peers
            .borrow_mut()
            .push(Peer { node, dist });
    }
}

struct Path {
    nodes: Vec<Rc<Node>>,
    dist: u32,
}

fn main() -> Result<(), String> {
    let file_name = std::env::args()
        .nth(1)
        .unwrap_or("day9".to_owned());
    let graph = parse(&file_name)?;
    let paths = find_paths(&graph);
    println!("Paths: {}", paths.len());

    if let Some(p) = paths.get(0) {
        println!("Path len: {}", p.dist);
    }

    Ok(())
}

fn find_paths(graph: &Vec<Rc<Node>>) -> Vec<Path> {
    let mut paths: Vec<Path> = graph.iter().flat_map(
        |start| walk(&start, &Path {
            nodes: vec!(start.clone()),
            dist: 0,
        }))
        .filter(|p| p.nodes.len() == graph.len())
        .collect();
    paths.sort_by_key(|p| p.dist);
    paths.reverse();
    paths
}

fn walk(start: &Rc<Node>, p: &Path) -> Vec<Path> {
    let mut paths = Vec::new();
    let peers = start.peers.borrow();
    let mut end = false;
    for peer in peers.iter() {
        if ! p.nodes.iter().any(|o| peer.node.name == o.name) {
            let mut new_path = Path {
                nodes: p.nodes.clone(),
                dist: p.dist + peer.dist,
            };
            new_path.nodes.push(peer.node.clone());
            paths.append(&mut walk(&peer.node, &new_path));

        }
        else {
            end = true;
        }
    }

    if end {
        paths.push(Path { nodes: p.nodes.clone(), dist: p.dist });
    }

    paths
}

fn parse(file_name: &str) -> Result<Vec<Rc<Node>>, String> {
    println!("filename: {}", file_name);
    let f = File::open(format!("testdata/{}.txt", file_name)).expect("Couldn't open file");
    let f = BufReader::new(f);

    let mut graph = HashMap::new();

    for line in f.lines() {
        match line {
            Err(_) => return Err("Something went wrong reading the file".to_string()),
            Ok(l) => {
                let tokens = l.split(' ').collect::<Vec<_>>();
                let a_name = tokens.get(0)
                    .ok_or("Couldn't find source")?
                    .to_string();
                let b_name = tokens.get(2)
                    .ok_or("Couldn't find dest")?
                    .to_string();
                let dist = tokens.get(4)
                    .ok_or("Couldn't find dist")?
                    .parse::<u32>()
                    .or(Err("Couldn't parse dist".to_string()))?;

                let a = graph.entry(a_name.clone())
                    .or_insert_with(|| Rc::new(Node::new(&a_name)))
                    .clone();

                let b = graph.entry(b_name.clone())
                    .or_insert_with(|| Rc::new(Node::new(&b_name)))
                    .clone();

                a.add_peer(b.clone(), dist);
                b.add_peer(a.clone(), dist);

                println!("{} {} {}", a.name, b.name, dist);
            },
        }
    }

    Ok(graph.values().map(|e| e.clone()).collect())
}
