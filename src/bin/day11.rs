fn main() -> Result<(), String> {
    println!(
        "Next password: {}",
        String::from_utf8(
            find_next_password("vzbxxyzz".as_bytes())
        ).or(Err("Couldn't parse utf8"))?
    );
    Ok(())
}

#[test]
fn test_find_next_password() {
    assert_eq!(
        String::from_utf8(find_next_password("abcdefgh".as_bytes())).unwrap(),
        "abcdffaa".to_string()
    );

    assert_eq!(
        String::from_utf8(find_next_password("ghijklmn".as_bytes())).unwrap(),
        "ghjaabcc".to_string()
    );
}

fn find_next_password(input: &[u8]) -> Vec<u8> {
    let mut next_pw: Vec<u8> = input.iter().map(|e| *e).collect();
    loop {
        next_pw = next(&next_pw);

        if valid(&next_pw) {
            break;
        }
    }

    next_pw
}

#[test]
fn test_valid() {
    assert_eq!(valid("abc".as_bytes()), false);
    assert_eq!(valid("abcdffaa".as_bytes()), true);
    assert_eq!(valid("hijklmmn".as_bytes()), false);
    assert_eq!(valid("abbceffg".as_bytes()), false);
    assert_eq!(valid("abbcegjk".as_bytes()), false);
}

/// Passwords must include one increasing straight of at least three letters, like abc, bcd,
/// cde, and so on, up to xyz. They cannot skip letters; abd doesn't count.
///
/// Passwords may not contain the letters i, o, or l, as these letters can be mistaken for
/// other characters and are therefore confusing.
///
/// Passwords must contain at least two different, non-overlapping pairs of letters,
/// like aa, bb, or zz.
///
fn valid(input: &[u8]) -> bool {
    input.len() == 8
        && is_all_lowercase(input)
        && ! contains_oil(input)
        && is_all_lowercase(input)
        && seq_of_3(input)
        && contains_two_seq(input)
}

#[test]
fn test_contains_oil() {
    assert_eq!(contains_oil("abc".as_bytes()), false);
    assert_eq!(contains_oil("oabc".as_bytes()), true);
    assert_eq!(contains_oil("aic".as_bytes()), true);
    assert_eq!(contains_oil("abl".as_bytes()), true);
}

fn contains_oil(input: &[u8]) -> bool {
    println!("Oil");
    input.iter().any(|c| *c == 105 || *c == 108 || *c == 111)
}

#[test]
fn test_contains_two_seq() {
    assert_eq!(contains_two_seq("abc".as_bytes()), false);
    assert_eq!(contains_two_seq("aac".as_bytes()), false);
    assert_eq!(contains_two_seq("abbb".as_bytes()), false);
    assert_eq!(contains_two_seq("aabaa".as_bytes()), true);
    assert_eq!(contains_two_seq("abcdffaa".as_bytes()), true);
}

fn contains_two_seq(input: &[u8]) -> bool {
    println!("two");
    let mut has_one_seq = false;
    let mut prev = input[0];
    let mut iter = input.iter();
    iter.next();
    loop {
        match iter.next() {
            Some(c) => {
                println!("C: {}, has one: {}, prev {}", *c, has_one_seq, prev);
                if has_one_seq && prev == *c {
                    return true;
                }

                if !has_one_seq {
                    has_one_seq = prev == *c;
                    println!("has one: {}, h: {}", *c, has_one_seq);
                    if has_one_seq {
                        match iter.next() {
                            Some(p) => prev = *p,
                            None    => {},
                        }
                    }
                    else {
                        prev = *c;
                    }
                }
                else {
                    prev = *c;
                }
            },
            None => break,
        }
    }

    false
}

#[test]
fn test_is_all_lowercase() {
    assert_eq!(is_all_lowercase("abc".as_bytes()), true);
    assert_eq!(is_all_lowercase("xyz".as_bytes()), true);
    assert_eq!(is_all_lowercase("aZzy".as_bytes()), false);
}

fn is_all_lowercase(input: &[u8]) -> bool {
    println!("lower");
    ! input.iter().any(|c| *c < 97u8 || *c > 122u8)
}

#[test]
fn test_seq_of_3() {
    assert_eq!(seq_of_3("abc".as_bytes()), true);
    assert_eq!(seq_of_3("xyz".as_bytes()), true);
    assert_eq!(seq_of_3("axyzy".as_bytes()), true);
    assert_eq!(seq_of_3("axzy".as_bytes()), false);
}

fn seq_of_3(input: &[u8]) -> bool {
    println!("three");
    let mut prev_1 = input[0];
    let mut prev_2 = input[1];
    println!("Testing: {:?}", input);

    input.iter()
        .skip(2)
        .any(|c| {
            if *c == prev_2 + 1 && prev_2 == prev_1 + 1 {
                return true;
            }
            else {
                prev_1 = prev_2;
                prev_2 = *c;
                return false;
            }
        })
}

#[test]
fn test_expand() {
    assert_eq!(next("aa".as_bytes()), vec!( 97, 98, ));
    assert_eq!(next("az".as_bytes()), vec!( 98, 97, ));
}

fn next(input: &[u8]) -> Vec<u8> {
    let mut new_value: Vec<u8> = input.iter().map(|e| *e).collect();
    new_value.reverse();
    for i in 0..new_value.len() {
        match new_value[i] {
            122 => new_value[i] = 97,
            _   => {
                new_value[i] += 1;
                break;
            },
        }
    }

    new_value.reverse();

    new_value
}
