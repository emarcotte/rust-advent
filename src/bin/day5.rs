use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file_name = std::env::args().nth(1).unwrap_or("day5-test".to_owned());
    println!("filename: {}", file_name);
    let f = File::open(format!("testdata/{}.txt", file_name)).expect("Couldn't open file");
    let f = BufReader::new(f);
    let count = f.lines()
        .filter(|e| match e { Ok(s) => is_nice(s), _ => false })
        .count();
    println!("Count: {}", count);
}

#[test]
fn test_is_nice() {
    assert!(is_nice("adaxad"));
    assert!(is_nice("ddaaaaoxodddd"));
    assert!(!is_nice("odo"));
    assert!(is_nice("qjhvhtzxzqqjkmpb"));
    assert!(is_nice("xxyxx"));
    assert!(is_nice("xxyxx"));
    assert!(is_nice("aaaxb1xb"));
    assert!(!is_nice("uurcxstgmygtbstg"));
    assert!(!is_nice("ieodomkazucvgmuy"));
}

fn is_nice(s: &str) -> bool {
    if s.len() < 4 {
        return false;
    }
    let mut has_single_gap = false;
    let mut has_repeat = false;
    let mut grand_prev = s.chars().nth(0).unwrap();
    let mut prev = s.chars().nth(1).unwrap();
    let mut search = String::with_capacity(2);

    for (i,e) in s.chars().enumerate().skip(2) {
        if e == grand_prev {
            has_single_gap = true;
        }

        if !has_repeat {
            search.replace_range(0.., &grand_prev.to_string());
            search.replace_range(1.., &prev.to_string());
            has_repeat = s.get(i..)
                .unwrap()
                .match_indices(&search)
                .nth(0).is_some();
        }

        if has_repeat && has_single_gap {
            return true;
        }

        grand_prev = prev;
        prev = e;
    }
    false
}
