use serde_json::{Value, };
use std::fs::File;

fn main() -> std::result::Result<(), String> {
    let f = File::open("testdata/day12.json")
        .or(Err("Couldn't open file?"))?;

    // Parse the string of data into serde_json::Value.
    let v: Value = match serde_json::from_reader(&f) {
        Err(e) => return Err(e.to_string()),
        Ok(v)  => v,
    };

    let n = find_numbers(&v)
        .iter()
        .fold(0f64, |a, b| a + b);

    println!("{:?}", n);

    Ok(())
}

fn find_numbers(v: &Value) -> Vec<f64> {
    let mut x = vec!();

    match v {
        Value::Array(a)  => a.iter().for_each(|inner| x.append(&mut find_numbers(inner))),
        Value::Object(m) => {
            let has_red = m.values().any(|v| match v {
                Value::String(s) => s == "red" ,
                _ => false, 
            });

            if !has_red {
                m.values().for_each(|inner| x.append(&mut find_numbers(inner)));
            }
        },
        Value::Number(n) => x.push(n.as_f64().unwrap()),
        Value::Bool(_)   => {},
        Value::String(_) => {},
        _                => {},
    };

    x
}
