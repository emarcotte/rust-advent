use std::fs::File;
use std::io::{Read};
use std::collections::{HashMap};

fn main() {
    let file_name = std::env::args().nth(1).unwrap_or("day3".to_owned());
    println!("filename: {}", file_name);
    let mut f = File::open(format!("testdata/{}.txt", file_name)).expect("Couldn't open file");
    let mut visits = HashMap::new();

    let mut positions = [
        (0, 0),
        (0, 0),
    ];

    let mut drunk_elf_logic = (0..positions.len()).cycle();

    let mut str = String::new();
    f.read_to_string(&mut str).expect("Couldn't read file");

    visits.entry((0, 0)).or_insert(2);

    for c in str.chars() {
        let mut pos = &mut positions[drunk_elf_logic.next().expect("Couldn't iterate!")];
        match c {
            '^' => pos.1 += 1,
            'v' => pos.1 -= 1,
            '>' => pos.0 += 1,
            '<' => pos.0 -= 1,
            _   => {},
        }
        visits.entry(*pos).and_modify(|e| *e += 1).or_insert(2);
    }

    println!("{}", visits.len());
}
